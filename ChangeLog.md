## 2020-09-24

* Add details about RemoveLiquidity and AddLiquidity.

## 2020-09-18

* Add details about how to use getAllowance with the view contract.

## 2020-05-12

* Fix slippage calculations and test data.

## 2020-04-20

* Update swap unit test files.
* Add README.md with details of how to setup the unit tests.
* Clarify why number values are stored as strings in the JSON files.
