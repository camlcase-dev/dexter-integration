# FA1.2 view entrypoints

View entrypoints are entrypoints that are intended for other contracts to call
and get data from the contract with view entyrypoints then bring that data back to the calling contract. 
The FA1.2 standard do not define a storage structure because some of these values may be 
calculated upon query and each implementation of FA1.2 may choose how they store or calculate
data. Instead there is a series of view entrypoints. All FA1.2 contracts need to expose the data
via the view entrypoints in the same way.

While view entrypoints are intended to be called another contract and used on-chain, 
we want to call them off-chain. This can be acheived by calling a generic 
intermediate contract and performing a dry run that fails and returns an error 
containing the value we want.

FA1.2 contracts have three view entrypoints.

- getAllowance:   ((address :owner, address :spender), contract nat)
- getBalance:     ((address :owner), contract nat)
- getTotalSupply: (unit, contract nat)

The first item of the tuple is the data sent to FA1.2 to perform the query
and the second item of the tuple is what will be sent back to the contract.
We want to get value from contract from the second item in the tuple.

In order to emulate a contract calling for the data, we will use the generic
[contract-view.tz](./contract-view.tz) contract. We will not discuss the details
of how it works, but you can look at [lorentz-contract-view](https://github.com/tqtezos/lorentz-contract-view)
to get a better understanding.

Originated copies are:
- delphinet: `KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR`
- mainnet: `KT1CPuTzwC7h7uLXd5WQmpMFso1HxrLBUtpE`

Here is how to call getBalance on testnet from the command line from the FA1.2 contract
`KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK` to look up the balance for 
`tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z`. Don't worry if it doesn't make sense, just make
sure it works. 

```base
tezos-client --wait none transfer 0 from alice to KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR --arg '{ PUSH mutez 0 ; NONE key_hash ; CREATE_CONTRACT { parameter nat ; storage unit ; code { FAILWITH } } ; DIP { DIP { LAMBDA (pair address unit) (pair (list operation) unit) { CAR ; CONTRACT nat ; IF_NONE { PUSH string "a" ; FAILWITH } {} ; PUSH address "tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z" ; PAIR ; DIP { PUSH address "KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK" ; CONTRACT %getBalance (pair address (contract nat)) ; IF_NONE { PUSH string "b" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS ; DIP { UNIT } ; PAIR } } ; APPLY ; DIP { PUSH address "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR" ; CONTRACT (lambda unit (pair (list operation) unit)) ; IF_NONE { PUSH string "c" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS } ; CONS ; DIP { UNIT } ; PAIR }' --dry-run
```

If you want details of the RPC calls, add the `-l` flag. The error will be returned in json format
from the `run_operation` RPC. You will need to copy two json files here to use them to call the
RPC from your application and get these values.

For the mainnet, replace any occurrence of `KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK` with 
`KT1CPuTzwC7h7uLXd5WQmpMFso1HxrLBUtpE`.

## get total supply

[fa1.2_get_total_supply.json](./fa1.2_get_total_supply.json) has two values that need to be replaced.

- `$contract_view_address`
- `$token_address`

## get balance

[fa1.2_get_balance.json](./fa1.2_get_balance.json) has three values that need to be replaced.

- `$contract_view_address`
- `$token_address`
- `$owner_address`

These jsons with the interpolated strings replaced will be used as the value for `parameters.value`
in the payload sent to the run_operation RPC. `parameters.entry` should be `getTotalSupply` or
`getBalance`. The `destination` should be `$contract_view_address`. You will need to fill in
`source`, `counter` and `chain_id` with appropriate values. The `signature` can be the empty signature 
`edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q`.
The `storage_limit` and `gas_limit` can be set to the max. The `amount` and `fee` can be zero.

A complete example for the POST body can be seen in [fa1.2_get_balance_example.json](./fa1.2_get_balance_example.json).

Here is what it looks like if we call it from curl.

```bash
curl --header "Content-Type: application/json" --request POST --data '{"operation":{"branch":"BL5PZjmQGw6kSMAmdhTeU47wBxCbA9atHPatRxFNTrZMBNR9oo5","contents":[{"kind":"transaction","source":"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV","fee":"0","counter":"0","gas_limit":"1040000","storage_limit":"60000","amount":"0","destination":"KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR","parameters":{"entrypoint":"default","value":[{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]},{"prim":"NONE","args":[{"prim":"key_hash"}]},{"prim":"CREATE_CONTRACT","args":[[{"prim":"parameter","args":[{"prim":"nat"}]},{"prim":"storage","args":[{"prim":"unit"}]},{"prim":"code","args":[[{"prim":"FAILWITH"}]]}]]},{"prim":"DIP","args":[[{"prim":"DIP","args":[[{"prim":"LAMBDA","args":[{"prim":"pair","args":[{"prim":"address"},{"prim":"unit"}]},{"prim":"pair","args":[{"prim":"list","args":[{"prim":"operation"}]},{"prim":"unit"}]},[{"prim":"CAR"},{"prim":"CONTRACT","args":[{"prim":"nat"}]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"a"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"address"},{"string":"tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z"}]},{"prim":"PAIR"},{"prim":"DIP","args":[[{"prim":"PUSH","args":[{"prim":"address"},{"string":"KT1U8kHUKJVcPkzjHLahLxmnXGnK1StAeNjK"}]},{"prim":"CONTRACT","args":[{"prim":"pair","args":[{"prim":"address"},{"prim":"contract","args":[{"prim":"nat"}]}]}],"annots":["%getBalance"]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"b"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]}]]},{"prim":"TRANSFER_TOKENS"},{"prim":"DIP","args":[[{"prim":"NIL","args":[{"prim":"operation"}]}]]},{"prim":"CONS"},{"prim":"DIP","args":[[{"prim":"UNIT"}]]},{"prim":"PAIR"}]]}]]},{"prim":"APPLY"},{"prim":"DIP","args":[[{"prim":"PUSH","args":[{"prim":"address"},{"string":"KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR"}]},{"prim":"CONTRACT","args":[{"prim":"lambda","args":[{"prim":"unit"},{"prim":"pair","args":[{"prim":"list","args":[{"prim":"operation"}]},{"prim":"unit"}]}]}]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"c"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]}]]},{"prim":"TRANSFER_TOKENS"},{"prim":"DIP","args":[[{"prim":"NIL","args":[{"prim":"operation"}]}]]},{"prim":"CONS"}]]},{"prim":"CONS"},{"prim":"DIP","args":[[{"prim":"UNIT"}]]},{"prim":"PAIR"}]}}],"signature":"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"},"chain_id":"NetXjD3HPJJjmcd"}' http://178.62.73.193:8732/chains/main/blocks/head/helpers/scripts/run_operation
```

# get allowance

[fa1.2_get_allowance.json](./fa1.2_get_allowance.json) has three values that need to be replaced.

- `$contract_view_address`
- `$token_address`
- `$owner_address`
- `$spender_address`

This is the same idea as get balance but it takes another parameter, the spender address.

It will return a natural number if the spender has an allowance for a given FA1.2 owner. 
If it does not it will fail with an unspecified error (this is not defined by the FA1.2
specification).

```
curl --header "Content-Type: application/json" --request POST --data '{"operation":{"branch":"BLPzAEPUEHF58wc46czKTDQME8DGcrvJEANSkNeqGoLBqNaFiMB","contents":[{"kind":"transaction","source":"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV","fee":"0","counter":"553001","gas_limit":"1040000","storage_limit":"60000","amount":"0","destination":"KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR","parameters":{"entrypoint":"default","value":[{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]},{"prim":"NONE","args":[{"prim":"key_hash"}]},{"prim":"CREATE_CONTRACT","args":[[{"prim":"parameter","args":[{"prim":"nat"}]},{"prim":"storage","args":[{"prim":"unit"}]},{"prim":"code","args":[[{"prim":"FAILWITH"}]]}]]},{"prim":"DIP","args":[[{"prim":"DIP","args":[[{"prim":"LAMBDA","args":[{"prim":"pair","args":[{"prim":"address"},{"prim":"unit"}]},{"prim":"pair","args":[{"prim":"list","args":[{"prim":"operation"}]},{"prim":"unit"}]},[{"prim":"CAR"},{"prim":"CONTRACT","args":[{"prim":"nat"}]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"a"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"address"},{"string":"tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"}]},{"prim":"PUSH","args":[{"prim":"address"},{"string":"tz1MQehPikysuVYN5hTiKTnrsFidAww7rv3z"}]},{"prim":"PAIR"},{"prim":"PAIR"},{"prim":"DIP","args":[[{"prim":"PUSH","args":[{"prim":"address"},{"string":"KT1UpGMT5arFH2wo7WczhntnawEisdZnsMzc"}]},{"prim":"CONTRACT","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address"},{"prim":"address"}]},{"prim":"contract","args":[{"prim":"nat"}]}]}],"annots":["%getAllowance"]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"b"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]}]]},{"prim":"TRANSFER_TOKENS"},{"prim":"DIP","args":[[{"prim":"NIL","args":[{"prim":"operation"}]}]]},{"prim":"CONS"},{"prim":"DIP","args":[[{"prim":"UNIT"}]]},{"prim":"PAIR"}]]}]]},{"prim":"APPLY"},{"prim":"DIP","args":[[{"prim":"PUSH","args":[{"prim":"address"},{"string":"KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR"}]},{"prim":"CONTRACT","args":[{"prim":"lambda","args":[{"prim":"unit"},{"prim":"pair","args":[{"prim":"list","args":[{"prim":"operation"}]},{"prim":"unit"}]}]}]},{"prim":"IF_NONE","args":[[{"prim":"PUSH","args":[{"prim":"string"},{"string":"c"}]},{"prim":"FAILWITH"}],[]]},{"prim":"PUSH","args":[{"prim":"mutez"},{"int":"0"}]}]]},{"prim":"TRANSFER_TOKENS"},{"prim":"DIP","args":[[{"prim":"NIL","args":[{"prim":"operation"}]}]]},{"prim":"CONS"}]]},{"prim":"CONS"},{"prim":"DIP","args":[[{"prim":"UNIT"}]]},{"prim":"PAIR"}]}}],"signature":"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"},"chain_id":"NetXjD3HPJJjmcd"}' http://178.62.73.193:8732/chains/main/blocks/head/helpers/scripts/run_operation
```
