https://usdtz.com/

6 decimals

https://usdtz.com/minteries.html

Minteries

Bakery-IL
https://tzbaker.co.il/
bakery-il@protonmail.com

Tezsure, Ltd.
https://tezsure.com/
info@tezsure.com

Tezos UK
community@tezos.org.uk



https://gitlab.com/tzip/tzip/-/tree/master/proposals/tzip-7

https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/ManagedLedger.md


## originate

To deploy ManagedLedger.tz, you have to originate the contract with the following 
initial value and it will be immediately usable:

```
Pair {} (Pair MANAGER_ADDR (Pair False 0))
```

Here MANAGER_ADDR is the address of the manager, 
False means that operations are not paused and 
0 is the initial total supply.

```
tezos-client originate contract USDtz transferring 0 from alice running ./usdtz.tz --init 'Pair {} (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" (Pair False 0))' --burn-cap 10 --force
```

KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY
19560

```
give 100,000 usdtz to alice

6 digits

(pair %mint (address :to) (nat :value)

tezos-client transfer 0 from alice to USDtz --entrypoint 'mint' --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 100000000000' --burn-cap 1

tezos-client transfer 0 from alice to USDtz --entrypoint 'burn' --arg 'Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" 1000000000000000000000' --burn-cap 1
```


Make a dexter contract for it 

```
tezos-client originate contract USDtzExchange transferring 0 from alice running ./dexter.tz --init 'Pair {} (Pair (Pair False (Pair False 0)) (Pair (Pair "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV" "KT1MDJpNo1nSznVkRCsphXwDrW3zHgZNjpzY") (Pair 0 0)))' --burn-cap 20 --force

KT1RtNatBzmk2AvJKm9Mx6b55GcQejJneK7t
19562
```


```
tezos-client transfer 0 from alice to USDtzExchange --entrypoint 'setManager' --arg '"KT1VoDHgfunTbRtYoPVZgcVQd2dvrTvyDfJd"' --burn-cap 1
```

camlCaseMultisig: KT1VoDHgfunTbRtYoPVZgcVQd2dvrTvyDfJd
