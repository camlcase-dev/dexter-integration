Originate a multisig with three signers 

```
tezos-client originate contract camlCaseMultisig transferring 0 from alice running ./generic_multisig.tz --init 'Pair 0 (Pair 2 {"edpkvZpfg8zysGRv3ZbHSi6n6aod7pGCd7fqAxLrDmghe5ZC1cueyR"; "edpktw7iTGpgX3K1LokkCFNmKqAVt83M33Vfa7MzCMcy4CYimjuVAs"; "edpkuiNt5MPfHBy77ccNvUFFGQtEcpnxiNHUMEuvcF1NzvYxEkqHrc"})' --burn-cap 10
```

tezos-client show address alice
tezos-client show address bob
tezos-client show address simon

do nothing (this one works!)

```
NetXjD3HPJJjmcd

tezos-client hash data 'Pair (Pair "NetXjD3HPJJjmcd" "KT1VoDHgfunTbRtYoPVZgcVQd2dvrTvyDfJd") (Pair 0 (Left { DROP; NIL operation; }))' of type 'pair (pair chain_id address) (pair :payload (nat %counter) (or (lambda unit (list operation)) (pair %change_keys (nat %threshold) (list %keys key))))'  | grep 'Raw packed data: ' | cut -d: -f2

0x05070707070a000000049caecab90a0000001601e8b51af2f2d6d9d0cfc8a479741f995d215fdcd00007070000050502000000060320053d036d


tezos-client sign bytes '0x05070707070a000000049caecab90a0000001601e8b51af2f2d6d9d0cfc8a479741f995d215fdcd00007070000050502000000060320053d036d' for alice | grep 'Signature: ' | cut -d: -f2
edsigu4sCbTrZrnvpAXLYDYeuHXFXF3fpd7ihBt1bZdfVRRDLhKx1FtmJmQcLJNM3r5LRY6BsJDdwnVjvFiXxZ19tHh2rmFu4C7

tezos-client sign bytes '0x05070707070a000000049caecab90a0000001601e8b51af2f2d6d9d0cfc8a479741f995d215fdcd00007070000050502000000060320053d036d' for bob | grep 'Signature: ' | cut -d: -f2
edsigttKFTu7oQyrVxqnn8jeVNMAkpZCtKmngnaMFAcoYgB6PEt6CSHzRyoEVoVM4dYzwXNxJ1iFKA8Seydb3YAggrcDJR2j34P



tezos-client transfer 0 from alice to camlCaseMultisig --entrypoint 'main' --arg 'Pair (Pair 0 (Left { DROP; NIL operation; })) {Some "edsigu4sCbTrZrnvpAXLYDYeuHXFXF3fpd7ihBt1bZdfVRRDLhKx1FtmJmQcLJNM3r5LRY6BsJDdwnVjvFiXxZ19tHh2rmFu4C7" ; Some "edsigttKFTu7oQyrVxqnn8jeVNMAkpZCtKmngnaMFAcoYgB6PEt6CSHzRyoEVoVM4dYzwXNxJ1iFKA8Seydb3YAggrcDJR2j34P" ; None }' --burn-cap 1 --dry-run

``pair (pair chain_id address) (pair :payload (nat %counter) <action>)``

```


sign data

```
tezos-client hash data 'Left { DROP; PUSH address "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"; CONTRACT unit; IF_NONE { PUSH string "NoDefaultEntryPoint"; FAILWITH } {}; PUSH mutez 0 ; UNIT ; TRANSFER_TOKENS ; DIP { NIL operation; }; CONS; }' of type '(or :action (lambda %operation unit (list operation)) (pair %change_keys (nat %threshold) (list %keys key)))'  | grep 'Raw packed data: ' | cut -d: -f2

0x050505020000006603200743036e0a000000160000471c8882bcf12586e640b7efa46c6ea1e0f4da9e0555036c072f020000001e0743036801000000134e6f44656661756c74456e747279506f696e74032702000000000743036a0000034f034d051f0200000004053d036d031b

tezos-client sign bytes '0x050505020000006603200743036e0a000000160000471c8882bcf12586e640b7efa46c6ea1e0f4da9e0555036c072f020000001e0743036801000000134e6f44656661756c74456e747279506f696e74032702000000000743036a0000034f034d051f0200000004053d036d031b' for alice | grep 'Signature: ' | cut -d: -f2

edsigtbe8htpTm5mojSMcNFR6rnp3aT9MufAr1zGUJQwCfLRmE1qWavouik1gwsr6NPFdqvnbzdwe2EhMeGNbhCvgbjBPaeFFs3

tezos-client sign bytes '0x050505020000006603200743036e0a000000160000471c8882bcf12586e640b7efa46c6ea1e0f4da9e0555036c072f020000001e0743036801000000134e6f44656661756c74456e747279506f696e74032702000000000743036a0000034f034d051f0200000004053d036d031b' for bob | grep 'Signature: ' | cut -d: -f2

edsigtwKa6YDrYC4zyfxD55kfK9MMhQgVKFsQy2CeDum3t9inh7ui4rgXYjqz7vJyCbSjvjAW4Cia1SipizQnHSdf35EMSURvxX

tezos-client transfer 0 from alice to camlCaseMultisig --entrypoint 'main' --arg 'Pair (Pair 0 (Left { DROP; PUSH address "tz1S82rGFZK8cVbNDpP1Hf9VhTUa4W8oc2WV"; CONTRACT unit; IF_NONE { PUSH string "NoDefaultEntryPoint"; FAILWITH } {}; PUSH mutez 0 ; UNIT ; TRANSFER_TOKENS ; DIP { NIL operation; }; CONS; })) {None ; Some "edsigtbe8htpTm5mojSMcNFR6rnp3aT9MufAr1zGUJQwCfLRmE1qWavouik1gwsr6NPFdqvnbzdwe2EhMeGNbhCvgbjBPaeFFs3" ; Some "edsigtwKa6YDrYC4zyfxD55kfK9MMhQgVKFsQy2CeDum3t9inh7ui4rgXYjqz7vJyCbSjvjAW4Cia1SipizQnHSdf35EMSURvxX"}' --burn-cap 1 --dry-run
```



```
tezos-client hash data 'Left { DROP; PUSH address "KT1FBmdCvWSgfUCeDg3Xg4LrDreV9TNayHGN"; CONTRACT %setBaker (pair (option key_hash) bool); IF_NONE { PUSH string "NoSetBakerEntrypoint" ; FAILWITH } {} ; PUSH mutez 0 ; PUSH bool False ; PUSH key_hash "tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B" ; SOME ; PAIR ; TRANSFER_TOKENS ; NIL operation ; SWAP ; CONS }' of type '(or :action (lambda %operation unit (list operation)) (pair %change_keys (nat %threshold) (list %keys key)))'  | grep 'Raw packed data: ' | cut -d: -f2

0x050505020000009b03200743036e0a0000001601486f5ce64c3190e0bb3044f8a6e24298c8858fd000065507650563035d0359000000092573657442616b6572072f020000001f0743036801000000144e6f53657442616b6572456e747279706f696e74032702000000000743036a00000743035903030743035d0a000000150081744d3ad071c4fd31913c5e80da1c6c9fb9f4b303460342034d053d036d034c031b

tezos-client sign bytes '0x050505020000009b03200743036e0a0000001601486f5ce64c3190e0bb3044f8a6e24298c8858fd000065507650563035d0359000000092573657442616b6572072f020000001f0743036801000000144e6f53657442616b6572456e747279706f696e74032702000000000743036a00000743035903030743035d0a000000150081744d3ad071c4fd31913c5e80da1c6c9fb9f4b303460342034d053d036d034c031b' for alice | grep 'Signature: ' | cut -d: -f2

edsigtwmXYdQhGwRJYwXsbyB9bvadEV9mSBY45T1LPWBDTGnWvjBbAhfa7xuZJ8QZtKzXLGAbDuGwNYEkrQm5kEYMzkdVMx6975

tezos-client sign bytes '0x050505020000009b03200743036e0a0000001601486f5ce64c3190e0bb3044f8a6e24298c8858fd000065507650563035d0359000000092573657442616b6572072f020000001f0743036801000000144e6f53657442616b6572456e747279706f696e74032702000000000743036a00000743035903030743035d0a000000150081744d3ad071c4fd31913c5e80da1c6c9fb9f4b303460342034d053d036d034c031b' for bob | grep 'Signature: ' | cut -d: -f2

edsigu2pHo64e6h9VJULyX28R2FuaQjNXr1PtAYfFKgzfZKfC2SCozTB9ojWa9zW5nsfhobjy4biy7tnE9fAKEYJPy1jomkzJir

tezos-client transfer 0 from alice to camlCaseMultisig --entrypoint 'main' --arg 'Pair (Pair 0 (Left { DROP; PUSH address "KT1FBmdCvWSgfUCeDg3Xg4LrDreV9TNayHGN"; CONTRACT %setBaker (pair (option key_hash) bool); IF_NONE { PUSH string "NoSetBakerEntrypoint" ; FAILWITH } {} ; PUSH mutez 0 ; PUSH bool False ; PUSH key_hash "tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B" ; SOME ; PAIR ; TRANSFER_TOKENS ; NIL operation ; SWAP ; CONS })) {None ; Some "edsigu2pHo64e6h9VJULyX28R2FuaQjNXr1PtAYfFKgzfZKfC2SCozTB9ojWa9zW5nsfhobjy4biy7tnE9fAKEYJPy1jomkzJir" ; Some "edsigtwmXYdQhGwRJYwXsbyB9bvadEV9mSBY45T1LPWBDTGnWvjBbAhfa7xuZJ8QZtKzXLGAbDuGwNYEkrQm5kEYMzkdVMx6975"}' --burn-cap 1 --dry-run
```






```
tezos-client hash data 'Left { DROP; PUSH address "KT1FBmdCvWSgfUCeDg3Xg4LrDreV9TNayHGN"; CONTRACT %setBaker (pair (option key_hash) bool); IF_NONE { PUSH string "NoSetBakerEntrypoint" ; FAILWITH } {} ; PUSH mutez 0 ; PUSH bool False ; PUSH key_hash "tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B" ; SOME ; PAIR ; TRANSFER_TOKENS ; NIL operation ; SWAP ; CONS }' of type '(or :action (lambda %operation unit (list operation)) (pair %change_keys (nat %threshold) (list %keys key)))'

tezos-client unpack michelson data 0x050505020000009b03200743036e0a0000001601486f5ce64c3190e0bb3044f8a6e24298c8858fd000065507650563035d0359000000092573657442616b6572072f020000001f0743036801000000144e6f53657442616b6572456e747279706f696e74032702000000000743036a00000743035903030743035d0a000000150081744d3ad071c4fd31913c5e80da1c6c9fb9f4b303460342034d053d036d034c031b

```
