# FA2 view entrypoints

For Dexter, there is one important view entrypoint.

- balance_of: `pair (list %requests (pair (address %owner) (nat %token_id))) (contract %callback (list (pair (pair %request (address %owner) (nat %token_id)) (nat %balance))))`

In order to emulate a contract calling for the data, we will use the generic
[contract-view.tz](./contract-view.tz) contract.

Originated copies of the contract-view contract are:
- delphinet: `KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR`
- mainnet: `KT1CPuTzwC7h7uLXd5WQmpMFso1HxrLBUtpE`

Here is how to call balance_of on testnet from the command line from the FA2 contract
`KT1SKWLsSfS66RBDYMoNi6rFGDpnsacH5BQN` to look up the balance for 
`tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5`. 

```bash
tezos-client --wait none transfer 0 from alicia to KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR --arg '{ PUSH mutez 0 ; NONE key_hash ; CREATE_CONTRACT { parameter (list (pair (pair address nat) nat)) ; storage unit ; code { FAILWITH } } ; DIP { DIP { LAMBDA (pair address unit) (pair (list operation) unit) { CAR ; CONTRACT (list (pair (pair address nat) nat)) ; IF_NONE { PUSH string "unexpected contract parameter" ; FAILWITH } {} ; NIL (pair address nat); PUSH nat 0; PUSH address "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" ; PAIR; CONS; PAIR ; DIP { PUSH address "KT1SKWLsSfS66RBDYMoNi6rFGDpnsacH5BQN" ; CONTRACT %balance_of (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) ; IF_NONE { PUSH string "unexpected contract parameter" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS ; DIP { UNIT } ; PAIR } } ; APPLY ; DIP { PUSH address "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR" ; CONTRACT (lambda unit (pair (list operation) unit)) ; IF_NONE { PUSH string "c" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS } ; CONS ; DIP { UNIT } ; PAIR }' --dry-run
```


## balance_of

### tezos-client

Replace the following values.

- `$fa2_contract`
- `$owner`
- `$token_id`

```bash
tezos-client --wait none transfer 0 from alicia to KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR --arg '{ PUSH mutez 0 ; NONE key_hash ; CREATE_CONTRACT { parameter (list (pair (pair address nat) nat)) ; storage unit ; code { FAILWITH } } ; DIP { DIP { LAMBDA (pair address unit) (pair (list operation) unit) { CAR ; CONTRACT (list (pair (pair address nat) nat)) ; IF_NONE { PUSH string "unexpected contract parameter" ; FAILWITH } {} ; NIL (pair address nat); PUSH nat $token_id; PUSH address "$owner" ; PAIR; CONS; PAIR ; DIP { PUSH address "fa2_contract" ; CONTRACT %balance_of (pair (list (pair address nat)) (contract (list (pair (pair address nat) nat)))) ; IF_NONE { PUSH string "unexpected contract parameter" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS ; DIP { UNIT } ; PAIR } } ; APPLY ; DIP { PUSH address "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR" ; CONTRACT (lambda unit (pair (list operation) unit)) ; IF_NONE { PUSH string "Contract type is incorrect" ; FAILWITH } {} ; PUSH mutez 0 } ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS } ; CONS ; DIP { UNIT } ; PAIR }' --dry-run
```

### curl

You must sign the data first and replace the $ values.

```bash
curl --header "Content-Type: application/json" --request POST --data '{ "operation": { "branch": "BLGKuRWocgh7hQATgmgF7wLq49QBDapZHTzKCGgqQ7SrkqXiUQH", "contents": [ { "kind": "transaction", "source": "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5", "fee": "0", "counter": "778411", "gas_limit": "1040000", "storage_limit": "60000", "amount": "0", "destination": "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR", "parameters": { "entrypoint": "default", "value": [ { "prim": "PUSH", "args": [ { "prim": "mutez" }, { "int": "0" } ] }, { "prim": "NONE", "args": [ { "prim": "key_hash" } ] }, { "prim": "CREATE_CONTRACT", "args": [ [ { "prim": "parameter", "args": [ { "prim": "list", "args": [ { "prim": "pair", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "nat" } ] }, { "prim": "nat" } ] } ] } ] }, { "prim": "storage", "args": [ { "prim": "unit" } ] }, { "prim": "code", "args": [ [ { "prim": "FAILWITH" } ] ] } ] ] }, { "prim": "DIP", "args": [ [ { "prim": "DIP", "args": [ [ { "prim": "LAMBDA", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "unit" } ] }, { "prim": "pair", "args": [ { "prim": "list", "args": [ { "prim": "operation" } ] }, { "prim": "unit" } ] }, [ { "prim": "CAR" }, { "prim": "CONTRACT", "args": [ { "prim": "list", "args": [ { "prim": "pair", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "nat" } ] }, { "prim": "nat" } ] } ] } ] }, { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "unexpected contract parameter" } ] }, { "prim": "FAILWITH" } ], [] ] }, { "prim": "NIL", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "nat" } ] } ] }, { "prim": "PUSH", "args": [ { "prim": "nat" }, { "int": "$token_id" } ] }, { "prim": "PUSH", "args": [ { "prim": "address" }, { "string": "$owner" } ] }, { "prim": "PAIR" }, { "prim": "CONS" }, { "prim": "PAIR" }, { "prim": "DIP", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "address" }, { "string": "$fa2_contract" } ] }, { "prim": "CONTRACT", "args": [ { "prim": "pair", "args": [ { "prim": "list", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "nat" } ] } ] }, { "prim": "contract", "args": [ { "prim": "list", "args": [ { "prim": "pair", "args": [ { "prim": "pair", "args": [ { "prim": "address" }, { "prim": "nat" } ] }, { "prim": "nat" } ] } ] } ] } ] } ], "annots": [ "%balance_of" ] }, { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "unexpected contract parameter" } ] }, { "prim": "FAILWITH" } ], [] ] }, { "prim": "PUSH", "args": [ { "prim": "mutez" }, { "int": "0" } ] } ] ] }, { "prim": "TRANSFER_TOKENS" }, { "prim": "DIP", "args": [ [ { "prim": "NIL", "args": [ { "prim": "operation" } ] } ] ] }, { "prim": "CONS" }, { "prim": "DIP", "args": [ [ { "prim": "UNIT" } ] ] }, { "prim": "PAIR" } ] ] } ] ] }, { "prim": "APPLY" }, { "prim": "DIP", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "address" }, { "string": "KT1BKjUEGQmb73DxvN2MBZ35n9Pc9jVfZhMR" } ] }, { "prim": "CONTRACT", "args": [ { "prim": "lambda", "args": [ { "prim": "unit" }, { "prim": "pair", "args": [ { "prim": "list", "args": [ { "prim": "operation" } ] }, { "prim": "unit" } ] } ] } ] }, { "prim": "IF_NONE", "args": [ [ { "prim": "PUSH", "args": [ { "prim": "string" }, { "string": "c" } ] }, { "prim": "FAILWITH" } ], [] ] }, { "prim": "PUSH", "args": [ { "prim": "mutez" }, { "int": "0" } ] } ] ] }, { "prim": "TRANSFER_TOKENS" }, { "prim": "DIP", "args": [ [ { "prim": "NIL", "args": [ { "prim": "operation" } ] } ] ] }, { "prim": "CONS" } ] ] }, { "prim": "CONS" }, { "prim": "DIP", "args": [ [ { "prim": "UNIT" } ] ] }, { "prim": "PAIR" } ] } } ], "signature": "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q" }, "chain_id": "NetXm8tYqnMWky1" }' http://178.62.73.193:8732/chains/main/blocks/head/helpers/scripts/run_operation
```
