# dexter-integration

Front ends that interface the Dexter smart contract should emulate the way that
Dexter calculates values in order to provide accurate predictions of swap 
returns for their users. In Dexter XTZ is a 64-bit integer value and token is
a natural number. There is currently no floating point arithmetic in Tezos smart 
contracts.

This project includes a collection of files for unit testing swap functions in 
whatever programming language we use. Number values are stored as strings for 
JavaScript compatibility.

## Division

`//` is integer division that rounds down. If there is a remainder it is discarded.

`ceildiv` is defined as the following:

```python
def ceildiv(n, d):
    q, r = divmod(n, d)
    return q + bool(r)
```
If the remained is 0 it is just the same as `//`. If it is greater than zero 
than round up.

## XTZ to Token

Our frontends should precisely calculate the amount of token returned from a
XTZ to token swap. Here is the implemenation in python.

```python
def xtzToToken(xtz_in, xtz_pool, token_pool):
    numerator = xtz_in * token_pool * 997
    denominator = xtz_pool * 1000 + xtz_in * 997
    return numerator // denominator
```

The file [xtz_to_token.json](./xtz_to_token.json) is used to faciliate unit 
tests in the chosen programming language. In contains an array of objects of the 
following format:

```json
{
    "xtz_pool": "1000000000",
    "token_pool": "250000",
    "xtz_in": "1000000",
    "token_out": "249"
}
```

Read each object and use `xtz_in`, `xtz_pool` and `token_pool` as parameters
of the function. The output should be equal to `token_out`.

## Token to XTZ

The frontend should precisely calculate the amount of XTZ returned from a
token to XTZ swap. Here is the implemenation in python.

```python
# calculate how much you get from a trade with fees
def tokenToXtz(token_in, xtz_pool, token_pool):
    numerator = token_in * xtz_pool * 997
    denominator = token_pool * 1000 + token_in * 997
    return numerator // denominator
```

The file [token_to_xtz.json](./token_to_xtz.json) is used to faciliate unit 
tests in the chosen programming language. In contains an array of objects of the 
following format:

```json
{
  "xtz_pool": "20000000",
  "token_pool": "1000",
  "token_in": "1000",
  "xtz_out": "9984977"
}
```

Read each object and use `token_in`, `xtz_pool` and `token_pool` as parameters
of the function. The output should be equal to `xtz_out`.

## Add Liquidity

Users add liquidity to a pool according to the balance of `xtz_pool` and 
`token_pool` in the Dexter contract.

There are two important values: the amount of XTZ you add `xtz_in` and the
maximum amount of tokens you add `max_tokens_deposited`. The amount of
tokens added depends on `xtz_pool`, `token_pool` and `xtz_in`.

If there is zero liquidity, `lqt_total == 0` then the amount of tokens
added is equivalent to `max_tokens_deposited`.

If there is non-zero liquidity, `lqt_total > 0` then the amount of tokens
added is calculated as follows.

```python
tokens_deposited = ceildiv(xtz_in * token_pool, xtz_pool);
```

## Remove Liquidity

Users who own liquidity can redeem (remove, burn) liquidity in order to get
XTZ and FA1.2 token in return. The important input values are `lqt_burned`,
`min_xtz_withdrawn` and `max_xtz_withdrawn`. Based on `lqt_burned`, 
`lqt_total`, `xtz_pool` and `token_pool`, Dexter will calculate how much 
XTZ and token will be returned. If either are less than the provided minimum 
amounts then the operation will fail.

The withdrawn amounts are calculated as follows:

```python
xtz_withdrawn    = lqt_burned * xtz_pool   // lqt_total
tokens_withdrawn = lqt_burned * token_pool // lqt_total
```

## Exchange Rate

The exchange rate is the ratio of a give input (XTZ or Token) and output 
(XTZ or token) after liquidity fees have been applied.

```python
def xtzToTokenExchangeRate(xtz_in, xtz_pool, token_pool):
    return xtzToToken(xtz_in, xtz_pool, token_pool) / xtz_in
```

For XTZ to Token display `1 XTZ = {xtzToTokenExchangeRate(...)} {Token Symbol}`.

```python
# exchange rate with fees
def tokenToXtzExchangeRate(token_in, xtz_pool, token_pool):
    return tokenToXtz(token_in, xtz_pool, token_pool) / token_in
```

For Token to XTZ display `1 {Token Symbol} = {tokenToXtzExchangeRate} XTZ`.

## Market Rate

The market rate is the raw ratio of the pools held by the Dexter contract.
It does not include the liquidity fees. We do not need to display the
market rate in the UI.

```python
def xtzToTokenMarketRate(xtz_pool, token_pool):
    return token_pool / xtz_pool

# exchange rate without fees
def tokenToXtzMarketRate(xtz_pool, token_pool):
    return xtz_pool / token_pool
```

## Slippage

Slippage is the difference between the market rate and the exchange rate.
The trader should expect some slippage but be allowed to limit it. If the
maximum allowed slippage is very small then it is likely that the user's 
trade will not go through. If the allowed slippage is very larg than the
user may get a lot less than the expected for a trade. In the UI we
display the slippage of the current exchange and the user's allowed slippage
rate.

```python
def xtzToTokenSlippage(xtz_in, xtz_pool, token_pool):
    exchange_rate = xtzToTokenExchangeRate(xtz_in, xtz_pool, token_pool)
    market_rate   = xtzToTokenMarketRate(xtz_pool, token_pool)
    return abs(exchange_rate - market_rate) / market_rate

def tokenToXtzSlippage(token_in, xtz_pool, token_pool):
    exchange_rate = tokenToXtzExchangeRate(token_in, xtz_pool, token_pool)
    market_rate   = tokenToXtzMarketRate(xtz_pool, token_pool)
    return abs(exchange_rate - market_rate) / market_rate
```

Setup a test for both [xtz_to_token.json](./xtz_to_token.json) and 
[token_to_xtz.json](./token_to_xtz.json) that uses the input values in those 
files for the functions xtzToTokenSlippage and tokenToXtzSlippage. They
should return float/double values which are within 0.0005 accuracy of the 
slippage value.

Example (both of these statements should be true):

```python
tokenToXtzSlippage(81, 17591539, 1019) > 0.0824 - 0.0005
tokenToXtzSlippage(81, 17591539, 1019) < 0.0824 + 0.0005
```

In Dexter contract front-end applications, the user can set the allowed slippage up to a 
hundredth of a percentage point (0.01% in percentage format or 0.0001 in decimal
formate).

## Minimum Expected Out

This is the minimum value that should be sent to the Dexter contract entrypoint 
xtzToToken and tokenToXtz. It is the minimum of XTZ or token that the trader will
receive. The value displayed in the inputs is the maximum amount the user could 
receive (if now slippage is applied at the time of trade). The logic is the
same for XTZ and Token (though the actual implementation will differ because
XTZ is Int64 and Token is Natural).

```python
def calculateMinimumOut(out, allowed_slippage):
    return out - out * allowed_slippage
```
